#include <stdio.h>
#include <unistd.h>
#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#define SIZE 4096
#define MAP_ANONYMOUS 0x20
#define MAP_FIXED_NOREPLACE 0x100000

static struct block_header *bh;
static struct block_header *all_bh;
static void *pointer_heap;

void test_1(){
  printf("Test 1:  start\n");
  pointer_heap = heap_init(SIZE);
  if(pointer_heap != NULL){
    bh = (struct block_header*) (pointer_heap);
    debug_heap(stdout, pointer_heap);
    printf("Test 1:  finished\n");
    return;
  }
  printf("Test 1:  failed\n");
}


void test_2(){
  printf("Test 2:  start\n");
  _free(all_bh);
  if(!bh -> is_free) printf("Test 2: failed\n");
  debug_heap(stdout, pointer_heap);
  printf("Test 2: finished\n");
  return;
}

void test_3(){
  printf("Test 3:  start\n");
  void *point_1 = _malloc(512);
  void *point_2 = _malloc(512);
  debug_heap(stdout, pointer_heap);
  _free(point_1);
  _free(point_2);
  if(!bh -> is_free) printf("Test 3: failed\n");
  debug_heap(stdout, pointer_heap);
  printf("Test 3: finished\n");
  return;
}

void test_4(){
  printf("Test 4:  start\n");
  struct block_header *bh_2;
  void *mem = _malloc(2*SIZE);
  debug_heap(stdout, pointer_heap);
  if (mem == NULL) printf("Test 4: failed\n");
  bh_2 = (struct block_header*) ((uint8_t*) mem - offsetof(struct block_header, contents));
  if(bh_2 -> capacity.bytes != 2*SIZE) printf("Test 4: failed\n");
  if(bh_2 -> is_free) printf("Test 4: failed\n");
  _free(mem);
  if(!bh_2 -> is_free) printf("Test 4: failed\n");
  debug_heap(stdout, pointer_heap);
  printf("Test 4: finished \n");
  return;
}

void test_5(){
    printf("Test 5:  start\n");
    struct block_header *current = bh;
    void *pointer;
    void *d1 = _malloc(20500);
    if (d1 == NULL) printf("Test 5: failed\n");
    
    while(current -> next != NULL){
      current = current -> next;
    }
    pointer = mmap((uint8_t*) current -> contents + current -> capacity.bytes, 20000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, 0, 0);
    
    if (pointer == NULL || pointer == MAP_FAILED) printf("Test 5: failed\n");
    debug_heap(stdout, pointer_heap);
    
    void *d2 = _malloc(90500);
    debug_heap(stdout, pointer_heap);
    struct block_header *bh_2 = (struct block_header*) ((uint8_t*) d2 - offsetof(struct block_header, contents));
    if ((uint8_t*) bh_2 == (uint8_t*) current -> contents + current -> capacity.bytes) printf("Test 5: failed\n");
    
    _free(d1);
    _free(d2);
     printf("Test 5: finished\n");
}

int main(){
  test_1();
  test_2();
  test_3();
  test_4();
  test_5();
  return 0;
}
